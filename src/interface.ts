export interface IVotingSubmit {
  topic_id: number
  vote_option: number
}
export interface IArgVote {
  typeIndex: number
  valStruct: IStructVote[]
}

export interface IStructVote {
  typeIndex: number
  valVec: ValVec[]
}

export interface ValVec {
  typeIndex: number
  valStruct: ValVecValStruct[]
}

export interface ValVecValStruct {
  typeIndex: number
  valPrimitive: number
}

export interface IVotingState {
  owner: Owner
  voting_topics: VotingTopic[]
  voting_results: any[]
  voting_deadline: VotingDeadline
}

export interface Owner {
  owner: string
}

export interface VotingDeadline {
  deadline_timstamp: DeadlineTimstamp
  is_finished: boolean
}

export interface DeadlineTimstamp {
  isSome: boolean
}

export interface VotingTopic {
  key: number
  value: Value
}

export interface Value {
  name: string
  description: Description
  voting_options: VotingOption[]
}

export interface Description {
  isSome: boolean
  innerValue?: string
}

export interface VotingOption {
  title: string
  additional_info: Description
}
