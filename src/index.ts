import { AbiParser, JsonValueConverter, StateReader } from 'abi-client-ts'
import assert from 'assert'
import { partisiaCrypto } from 'partisia-crypto'
import { PartisiaAccount, PartisiaRpc } from 'partisia-rpc'
import { IPartisiaRpcConfig, PartisiaAccountClass } from 'partisia-rpc/lib/main/accountInfo'
import { PartisiaRpcClass } from 'partisia-rpc/lib/main/rpc'
import { IArgVote, IVotingState, IVotingSubmit } from './interface'
import serializePulicContract from './serializePulicContract'

// const abi = Buffer.from(
//   'UEJDQUJJBwAABAEAAAAACAAAAAtWb3RpbmdUb3BpYwAAAAMAAAAEbmFtZQsAAAALZGVzY3JpcHRpb24SCwAAAA52b3Rpbmdfb3B0aW9ucw4AAQAAAAxWb3RpbmdPcHRpb24AAAACAAAABXRpdGxlCwAAAA9hZGRpdGlvbmFsX2luZm8SCwAAAA5Wb3RpbmdEZWFkbGluZQAAAAIAAAARZGVhZGxpbmVfdGltc3RhbXASBAAAAAtpc19maW5pc2hlZAwAAAALVm90aW5nU3RhdGUAAAAEAAAABW93bmVyAAcAAAANdm90aW5nX3RvcGljcw8BAAAAAAAOdm90aW5nX3Jlc3VsdHMPAQ8NAQAAAA92b3RpbmdfZGVhZGxpbmUAAgAAAA1Wb3RpbmdJbml0TXNnAAAAAwAAAAVvd25lchINAAAADXZvdGluZ190b3BpY3MOAAAAAAAPdm90aW5nX2RlYWRsaW5lEgQAAAAHVm90ZU1zZwAAAAEAAAALc3VibWlzc2lvbnMOAAYAAAAPVG9waWNTdWJtaXNzaW9uAAAAAgAAAAh0b3BpY19pZAEAAAAPc2VsZWN0ZWRfb3B0aW9uAQAAABBPd25hYmxlQmFzZVN0YXRlAAAAAQAAAAVvd25lcg0AAAADAQAAAAppbml0aWFsaXpl/////w8AAAABAAAAA21zZwAEAgAAAAR2b3RlAQAAAAEAAAADbXNnAAUCAAAAE2ZpbmlzaF92b3RpbmdfcGhhc2UDAAAAAAAD',
//   'base64'
// )
// const abi = Buffer.from(
//   'UEJDQUJJBwAABAEAAAAACAAAAAtWb3RpbmdUb3BpYwAAAAMAAAAEbmFtZQsAAAALZGVzY3JpcHRpb24SCwAAAA52b3Rpbmdfb3B0aW9ucw4AAQAAAAxWb3RpbmdPcHRpb24AAAACAAAABXRpdGxlCwAAAA9hZGRpdGlvbmFsX2luZm8SCwAAAA5Wb3RpbmdEZWFkbGluZQAAAAIAAAARZGVhZGxpbmVfdGltc3RhbXASBAAAAAtpc19maW5pc2hlZAwAAAALVm90aW5nU3RhdGUAAAAEAAAABW93bmVyAAcAAAANdm90aW5nX3RvcGljcw8BAAAAAAAOdm90aW5nX3Jlc3VsdHMPAQ8NAQAAAA92b3RpbmdfZGVhZGxpbmUAAgAAAA1Wb3RpbmdJbml0TXNnAAAAAwAAAAVvd25lchINAAAADXZvdGluZ190b3BpY3MOAAAAAAAPdm90aW5nX2RlYWRsaW5lEgQAAAAHVm90ZU1zZwAAAAEAAAALc3VibWlzc2lvbnMOAAYAAAAPVG9waWNTdWJtaXNzaW9uAAAAAgAAAAh0b3BpY19pZAEAAAAPc2VsZWN0ZWRfb3B0aW9uAQAAABBPd25hYmxlQmFzZVN0YXRlAAAAAQAAAAVvd25lcg0AAAAEAQAAAAppbml0aWFsaXpl/////w8AAAABAAAAA21zZwAEAgAAAAR2b3RlAQAAAAEAAAADbXNnAAUCAAAAE2ZpbmlzaF92b3RpbmdfcGhhc2UDAAAAAAIAAAARcmVzZXRfdm90ZXNfc3RhdGUFAAAAAAAD',
//   'base64'
// )
export class VotingContract {
  abi?: string
  contractAddress: string
  rpc: PartisiaAccountClass

  constructor(contractAddress: string, rpc: IPartisiaRpcConfig, abi?: string) {
    this.abi = abi
    this.contractAddress = contractAddress
    this.rpc = PartisiaAccount(rpc)
  }
  async initAbi() {
    const contract = await this.rpc.getContract(this.contractAddress, this.rpc.deriveShardId(this.contractAddress), true)
    this.abi = contract.data.abi
  }
  async actionVote(args: IVotingSubmit[]) {
    // serialize the payload
    if (!this.abi) await this.initAbi()

    const fileAbi = new AbiParser(Buffer.from(this.abi, 'base64')).parseAbi()
    const fnAbi = fileAbi.contract.getFunctionByName('vote')

    const argVote: IArgVote = {
      typeIndex: 0,
      valStruct: [
        {
          typeIndex: 14,
          valVec: args.map((a) => {
            return {
              typeIndex: 0,
              valStruct: [
                { typeIndex: 1, valPrimitive: a.topic_id },
                { typeIndex: 1, valPrimitive: a.vote_option },
              ],
            }
          }),
        },
      ],
    }

    const payload = serializePulicContract(fnAbi, fileAbi.contract, [argVote])
    return payload
  }
  async getVotingState(): Promise<IVotingState> {
    if (!this.abi) await this.initAbi()
    const contract = await this.rpc.getContract(this.contractAddress, this.rpc.deriveShardId(this.contractAddress), true)

    // deserialize state
    const fileAbi = new AbiParser(Buffer.from(this.abi, 'base64')).parseAbi()
    const reader = new StateReader(Buffer.from(contract.data.serializedContract!.state.data, 'base64'), fileAbi.contract)
    const struct = reader.readStruct(fileAbi.contract.getStateStruct())
    return JsonValueConverter.toJson(struct) as any
  }
  async castVotes(
    privateKey: string | Buffer,
    votes: IVotingSubmit[],
    isMainnet?: boolean,
    cost: number | string = 40960
  ): Promise<{
    isFinalOnChain: boolean
    trxHash: string
    hasError: boolean
    errorMessage?: string
  }> {
    const wif = typeof privateKey === 'string' ? privateKey : privateKey.toString('hex')
    const address = partisiaCrypto.wallet.privateKeyToAccountAddress(wif)

    const shardId = this.rpc.deriveShardId(address)
    const url = this.rpc.getShardUrl(shardId)
    const nonce = await this.rpc.getNonce(address, shardId)

    const payload = await this.actionVote(votes)
    const serializedTransaction = partisiaCrypto.transaction.serializedTransaction(
      {
        nonce,
        cost,
      },
      {
        contract: this.contractAddress,
      },
      payload
    )
    const digest = partisiaCrypto.transaction.deriveDigest(`Partisia Blockchain${isMainnet ? '' : ' Testnet'}`, serializedTransaction)
    const signature = partisiaCrypto.wallet.signTransaction(digest, wif)
    const trx = partisiaCrypto.transaction.getTransactionPayloadData(serializedTransaction, signature)

    const trxHash = partisiaCrypto.transaction.getTrxHash(digest, signature)
    const rpcShard = PartisiaRpc({ baseURL: url })
    const isValid = await rpcShard.broadcastTransaction(trx)
    assert(isValid, 'Unknown Error')

    const isFinalOnChain = await broadcastTransactionPoller(trxHash, rpcShard)

    // check for errors
    let res
    if (isFinalOnChain) {
      res = await this.rpc.getTransactionEventTrace(trxHash)
    } else {
      res = {
        hasError: true,
        errorMessage: 'unable to broadcast to chain',
      }
    }

    return {
      isFinalOnChain,
      trxHash,
      ...res,
    }
  }
}

export const broadcastTransactionPoller = async (trxHash: string, rpc: PartisiaRpcClass, num_iter = 10, interval_sleep = 2000) => {
  let intCounter = 0
  while (++intCounter < num_iter) {
    try {
      console.log('intCounter', intCounter)
      const resTx = await rpc.getTransaction(trxHash)
      if (resTx.finalized) {
        break
      }
    } catch (error) {
      console.error(error.message)
    } finally {
      const sleep = (ms) => {
        return new Promise((resolve) => setTimeout(resolve, ms))
      }
      await sleep(interval_sleep)
    }
  }
  return intCounter < num_iter
}
