### Voting Contract JavaScript Interface
Source WASM and ABI for contract: `https://github.com/partisiablockchainapplications/public-voting`
Example testnet deploy: `https://testnet.mpcexplorer.com/address/0283e6302623d997b1fafd8fdb4870719f290e9c27`


### Example Deserialize Contract State
```ts
;(async () => {
  const addressContract = '0283e6302623d997b1fafd8fdb4870719f290e9c27'
  const rpcConfig = {
    urlBaseGlobal: { url: 'https://node1.testnet.partisiablockchain.com', shard_id: 99 },
    urlBaseShards: [
      { url: 'https://node1.testnet.partisiablockchain.com/shards/Shard0', shard_id: 0 },
      { url: 'https://node1.testnet.partisiablockchain.com/shards/Shard1', shard_id: 1 },
      { url: 'https://node1.testnet.partisiablockchain.com/shards/Shard2', shard_id: 2 },
    ],
  }
  const votingContract = new VotingContract(addressContract, rpcConfig)
  const state = await votingContract.getVotingState()
  console.log(state)
})()
```

### Example Cast Vote directly on Chain with Private Key
```ts
;(async () => {
  const PRIVATE_KEY = '<64 len hex string>'
  const addressContract = '0283e6302623d997b1fafd8fdb4870719f290e9c27'
  const rpcConfig = {
    urlBaseGlobal: { url: 'https://node1.testnet.partisiablockchain.com', shard_id: 99 },
    urlBaseShards: [
      { url: 'https://node1.testnet.partisiablockchain.com/shards/Shard0', shard_id: 0 },
      { url: 'https://node1.testnet.partisiablockchain.com/shards/Shard1', shard_id: 1 },
      { url: 'https://node1.testnet.partisiablockchain.com/shards/Shard2', shard_id: 2 },
    ],
  }
  const votingContract = new VotingContract(addressContract, rpcConfig)
  // define the config

  const votes = [
    { topic_id: 0, vote_option: 0 },
    { topic_id: 1, vote_option: 0 },
    { topic_id: 2, vote_option: 4 },
    { topic_id: 3, vote_option: 1 },
    { topic_id: 4, vote_option: 1 },
  ]

  // actually cast the vote
  const res = await votingContract.castVotes(PRIVATE_KEY, votes, false)
  console.log('res', JSON.stringify(res, null, 2))
  // Expected output:
  /* 
  {
    "isFinalOnChain": true,
    "trxHash": "97f5a53e359423a0fe4c03695d7db5689aac19f4f459fd3ff520e5a130ab6b45",
    "hasError": false,
    "eventTrace": [
        {
        "shardId": 1,
        "txHash": "dcc4c0393fb21ae71d09f20721113e528d61b0df116b1a2439ca0f2c737a2cae"
        }
    ]
  } */
})()
```